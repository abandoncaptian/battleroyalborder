package me.abandoncaptian.BattleRoyalBorder;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.WorldBorder;

public class BRBAPI {
	private ArrayList<BRBorder> borders = new ArrayList<BRBorder>();
	private static BRBAPI instance;
	public BRBAPI(){
		instance = this;
	}
	
	public static BRBAPI getAPI(){
		return instance;
	}
	
	public BRBorder createBorder(World world, Location center, double radius){
		BRBorder border = new BRBorder(world, center, radius);
		borders.add(border);
		return border;
	}
	
	public BRBorder createBorder(World world, WorldBorder worldBorder){
		BRBorder border = new BRBorder(world, worldBorder);
		borders.add(border);
		return border;
	}
	
	public BRBorder createBorder(World world, double centerX, double centerZ, double radius){
		BRBorder border = new BRBorder(world, centerX, centerZ, radius);
		borders.add(border);
		return border;
	}
	
	public BRBorder getBorder(World world){
		if(borders.isEmpty())return null;
		for(BRBorder b : borders){
			if(b.getWorld().getName().equals(world.getName()))return b;
		}
		return null;
	}
}
