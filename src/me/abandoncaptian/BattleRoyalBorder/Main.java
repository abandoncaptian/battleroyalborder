package me.abandoncaptian.BattleRoyalBorder;

import java.util.logging.Logger;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

public class Main extends JavaPlugin implements Listener{
	Logger Log = Bukkit.getLogger();
	static Main instance;
	@Override
	public void onEnable()
	{
		instance = this;
		new BRBAPI();
		Log.info("------- [ BattleRoyalBorder API v1.2 ] -------");
		Log.info(" ");
		Log.info("                   Enabled!");
		Log.info(" ");
		Log.info("----------------------------------------------");
	}
	
	static Main getMain(){
		return instance;
	}

}